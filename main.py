import pandas as pd
from delab_trees import TreeManager


def load_data():
    # Use a breakpoint in the code line below to debug your script.
    df = pd.read_csv("posts.csv")
    print(df.head())
    return df


def load_data_into_delab_trees(df):
    df.rename(columns={"twitter_id": "post_id", "conversation_id": "tree_id", "tn_parent_id": "parent_id"},
              inplace=True)
    df["post_id"] = df["post_id"].astype(float).astype(str)
    df["parent_id"] = df["parent_id"].astype(float).astype(str)
    manager = TreeManager(df)
    manager.remove_invalid()
    manager.validate(verbose=True)
    return manager


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # load csv into pandas
    df = load_data()
    # load pandas into tree_manager
    tree_manager = load_data_into_delab_trees(df)
    # get an example conversation
    tree = tree_manager.random()
    # print the conversation_tree
    print("##### Printing TREE")
    print(tree.as_recursive_tree().to_string())
    # get conversation_flows with length 5
    flows = tree_manager.get_flow_sample(5)
    # get example flow
    example_flow = flows[0]
    print("##### Printing FLOW")
    print(*list(map(lambda x: x.text, example_flow)), sep="\n")


